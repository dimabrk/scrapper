package parse

import "database/sql"

type ErrorParser interface {
	ParseToSqlNullString(error) sql.NullString
}

type Error struct {
	err error
}

func NewErrorParse(err error) *Error {
	return &Error{err: err}
}

func (e *Error) ParseToSqlNullString() sql.NullString {
	if e.err == nil {
		return sql.NullString{}
	}
	return sql.NullString{
		String: e.err.Error(),
		Valid:  true,
	}
}
