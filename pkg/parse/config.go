package parse

import (
	"encoding/json"
	"os"
	"scrapper/configs"
)

type ConfigParser interface {
	ParseJsonConfig(string, *configs.Сonfiguration) error
}

type Config struct {
	conf *configs.Сonfiguration
	path string
}

func NewConfigParse(path string, conf *configs.Сonfiguration) *Config {
	return &Config{
		conf: conf,
		path: path}
}

func (c *Config) ParseJsonConfig() error {
	file, _ := os.Open(c.path)
	decoder := json.NewDecoder(file)
	err := decoder.Decode(&c.conf)
	if err != nil {
		return err
	}
	defer file.Close()
	return nil
}

