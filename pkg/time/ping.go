package time

import (
	"time"
)

type Pinger interface {
	GetPing(string) (time.Duration, error)
}

type Ping struct {
	Pinger
}

func NewPinger() *Ping {
	return &Ping{Pinger: NewMeasurement()}
}
