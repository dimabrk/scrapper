package time

import (
	"fmt"
	"net/http"
	"time"
)

type Measurement struct {
}

func NewMeasurement() *Measurement {
	return &Measurement{}
}

func (m *Measurement) GetPing(serviceName string) (time.Duration, error) {
	start := time.Now()
	resp, err := http.Get("https://" + serviceName)
	duration := time.Since(start)

	if err != nil {
		fmt.Println("Ошибка соединения.", err)
		return 0, err
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		fmt.Println("Ошибка. http-статус: ", resp.StatusCode)
		return 0, err
	}

	return duration, nil
}
