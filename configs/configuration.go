package configs

type Сonfiguration struct {
	Port           string
	DbUserName     string
	DbPassword     string
	DbName         string
	DbSslMode      string
	DbHost         string
	DbPort         string
	Offset         int `json:"AutoCheckFrom"`
	Limit          int `json:"AutoCheckCount"`
	RoutineLimited int
}
