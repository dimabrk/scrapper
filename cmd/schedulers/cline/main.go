package main

import (
	"github.com/claudiu/gocron"
	"log"
	"scrapper/configs"
	"scrapper/internal/repository"
	"scrapper/internal/repository/pgSql"
	"scrapper/internal/service"
	"scrapper/pkg/parse"
)

const configPath = "configs/config.json"

var services *service.Service

func main() {
	log.Println("scheduler 'cline' started")
	c := configs.Сonfiguration{}
	if err := parse.NewConfigParse(configPath, &c).ParseJsonConfig(); err != nil {
		log.Fatalf("Error init config: %s", err.Error())
	}
	db, err := pgSql.NewPostgresDb(pgSql.Config{
		UserName: c.DbUserName,
		Password: c.DbPassword,
		DbName:   c.DbName,
		SslMode:  c.DbSslMode,
		Host:     c.DbHost,
		Port:     c.DbPort,
	})
	if err != nil {
		log.Fatalf("Error to connect database: %s", err.Error())
	}
	rep := repository.NewRepository(db, c.Limit, c.Offset)
	services = service.NewService(rep)
	s := gocron.NewScheduler()
	s.Every(1).Monday().Do(removeOldStatistic)
	<-s.Start()
}

func removeOldStatistic() {
	err := services.RemoveOldStatistic()
	if err != nil {
		log.Fatalf("Error while remove old sttistic: %s", err.Error())
	}
}
