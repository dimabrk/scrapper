package main

import (
	"github.com/claudiu/gocron"
	"log"
	"scrapper/configs"
	"scrapper/internal/repository"
	"scrapper/internal/repository/pgSql"
	"scrapper/internal/service"
	"scrapper/pkg/parse"
	"scrapper/pkg/time"
)

const configPath = "configs/config.json"

var services *service.Service
var routineLimited int
var pinger *time.Ping

func main() {
	log.Println("scheduler 'ping' started")
	c := configs.Сonfiguration{}
	pinger = time.NewPinger()
	if err := parse.NewConfigParse(configPath, &c).ParseJsonConfig(); err != nil {
		log.Fatalf("Error init config: %s", err.Error())
	}
	routineLimited = c.RoutineLimited
	db, err := pgSql.NewPostgresDb(pgSql.Config{
		UserName: c.DbUserName,
		Password: c.DbPassword,
		DbName:   c.DbName,
		SslMode:  c.DbSslMode,
		Host:     c.DbHost,
		Port:     c.DbPort,
	})
	if err != nil {
		log.Fatalf("Error to connect data base: %s", err.Error())
	}
	repo := repository.NewRepository(db, c.Limit, c.Offset)
	services = service.NewService(repo)

	s := gocron.NewScheduler()
	s.Every(1).Minutes().Do(checkServices)
	<-s.Start()
}

func checkServices() error {
	channelCounter := make(chan bool, routineLimited)
	services, err := services.GetServices()
	if err != nil {
		log.Fatalf("Error while getting list services: %s", err.Error())
		return err
	}
	for i := range services {
		select {
		case channelCounter <- true:
			go startRoutine(channelCounter, services[i])
		}
	}
	return nil
}

func startRoutine(channelCounter chan bool, serviceName string) {
	ping, err := pinger.GetPing(serviceName)

	services.UpdateServices(serviceName, ping.Nanoseconds(), err)

	<-channelCounter
}
