package main

import (
	"log"
	"scrapper/configs"
	"scrapper/internal/delivery"
	"scrapper/internal/repository"
	"scrapper/internal/repository/pgSql"
	"scrapper/internal/server"
	"scrapper/internal/service"
	"scrapper/pkg/parse"
)

// @title Checker service API
// @version 1.0
// @description This is API for check services and getting information

// @host localhost:8080
// @BasePath /

const configPath = "configs/config.json"

func main() {
	config := configs.Сonfiguration{}
	if err := parse.NewConfigParse(configPath, &config).ParseJsonConfig(); err != nil {
		log.Fatalf("Error init config: %s", err.Error())
	}

	db, err := pgSql.NewPostgresDb(pgSql.Config{
		UserName: config.DbUserName,
		Password: config.DbPassword,
		DbName:   config.DbName,
		SslMode:  config.DbSslMode,
		Host:     config.DbHost,
		Port:     config.DbPort,
	})
	if err != nil {
		log.Fatalf("Error to connect database: %s", err.Error())
	}
	repo := repository.NewRepository(db, config.Limit, config.Offset)
	services := service.NewService(repo)
	handlers := delivery.NewDelivery(services).HttpHandler
	srv := server.NewServer().HttpServer
	if err := srv.Run(config.Port, handlers.InitRoutes()); err != nil {
		log.Fatalf("Error! server not run: %s", err.Error())
	}
}
