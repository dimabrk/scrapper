package service

import "scrapper/internal/repository"

type PingInfo struct {
	repo repository.ToDoPingInfo
}

func NewPingInfoService(repo repository.ToDoPingInfo) *PingInfo {
	return &PingInfo{repo: repo}
}

func (p *PingInfo) GetMaxPing() (string, error) {
	return p.repo.GetMaxPing()
}

func (p *PingInfo) GetMinPing() (string, error) {
	return p.repo.GetMinPing()
}
