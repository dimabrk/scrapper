package service

import (
	"scrapper/internal/repository"
	"scrapper/internal/repository/types"
)

type ToDoStatistic interface {
	GetErrorServices() ([]types.ErrorService, error)
	GetCheckStatistic() ([]types.CheckStatistic, error)
	RemoveOldStatistic() error
}

type ToDoPingInfo interface {
	GetMaxPing() (string, error)
	GetMinPing() (string, error)
}

type ToDoService interface {
	ServiceIsSupport(string) (bool, error)
	GetServices() ([]string, error)
	UpdateCheckServiceInfo(string) error
	UpdateServices(string, int64, error) error
}

type Service struct {
	ToDoPingInfo
	ToDoStatistic
	ToDoService
}

func NewService(repos *repository.Repository) *Service {
	return &Service{
		ToDoPingInfo:  NewPingInfoService(repos),
		ToDoService:   NewServiceInfoService(repos),
		ToDoStatistic: NewStatistic(repos)}
}
