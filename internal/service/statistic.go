package service

import (
	"scrapper/internal/repository"
	"scrapper/internal/repository/types"
)

type Statistic struct {
	repo repository.ToDoStatistic
}

func NewStatistic(repo repository.ToDoStatistic) *Statistic {
	return &Statistic{repo: repo}
}

func (s *Statistic) GetErrorServices() ([]types.ErrorService, error) {
	return s.repo.GetErrorServices()
}

func (s *Statistic) GetCheckStatistic() ([]types.CheckStatistic, error) {
	return s.repo.GetCheckStatistic()
}
func (s *Statistic) RemoveOldStatistic() error {
	return s.repo.RemoveOldStatistic()
}
