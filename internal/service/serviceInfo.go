package service

import "scrapper/internal/repository"

type ServiceInfo struct {
	repo repository.ToDoService
}

func NewServiceInfoService(repo repository.ToDoService) *ServiceInfo {
	return &ServiceInfo{repo: repo}
}

func (p *ServiceInfo) ServiceIsSupport(service string) (bool, error) {
	return p.repo.ServiceIsSupport(service)
}

func (p *ServiceInfo) GetServices() ([]string, error) {
	return p.repo.GetServices()
}

func (p *ServiceInfo) UpdateCheckServiceInfo(service string) error {
	return p.repo.UpdateCheckServiceInfo(service)
}

func (p *ServiceInfo) UpdateServices(serviceName string, ping int64, errorDesc error) error {
	return p.repo.UpdateServices(serviceName, ping, errorDesc)
}
