package pgSql

import (
	"database/sql"
	"fmt"
	"scrapper/pkg/parse"
	"time"
)

type ServiceInfo struct {
	db     *sql.DB
	limit  int
	offset int
}

func NewServiceInfoPg(db *sql.DB, limit int, offset int) *ServiceInfo {
	return &ServiceInfo{
		db:     db,
		limit:  limit,
		offset: offset}
}

func (s *ServiceInfo) ServiceIsSupport(serviceName string) (bool, error) {
	row := s.db.QueryRow(
		"select exists("+
			"select 1 "+
			"from servises "+
			"where name_service = $1)", serviceName)
	var isSupport bool
	if err := row.Scan(&isSupport); err != nil {
		return false, err
	}
	return isSupport, nil
}

func (s *ServiceInfo) GetServices() ([]string, error) {
	var rows *sql.Rows
	var err error

	if s.limit < 0 {
		rows, err = s.db.Query(
			"SELECT name_service "+
				"FROM servises "+
				"ORDER BY id "+
				"OFFSET $1", s.offset)
	} else {
		rows, err = s.db.Query(
			"SELECT name_service "+
				"FROM servises "+
				"ORDER BY id "+
				"LIMIT $1 OFFSET $2", s.limit, s.offset)
	}

	if err != nil {
		return nil, err
	}

	var services []string

	for rows.Next() {
		service := ""
		err := rows.Scan(&service)
		if err != nil {
			fmt.Println(err)
			continue
		}
		services = append(services, service)
	}

	return services, nil
}

func (s *ServiceInfo) UpdateCheckServiceInfo(serviceName string) error {
	row := s.db.QueryRow("select exists(select 1 "+
		"from check_services_info "+
		"where service_name = $1)", serviceName)
	var exist bool
	var needInsert = true

	if err := row.Scan(&exist); err != nil {
		panic(err)
	}

	if exist {
		row := s.db.QueryRow("SELECT id, day, count_click "+
			"FROM check_services_info "+
			"WHERE service_name = $1  "+
			"ORDER BY day DESC "+
			"LIMIT 1", serviceName)

		var id int64
		var day time.Time
		var countClick int64

		if err := row.Scan(&id, &day, &countClick); err != nil {
			return err
		}

		ay, am, ad := day.Date()
		dbDate := fmt.Sprintf("%d%d%d", ay, am, ad)

		by, bm, bd := time.Now().Date()
		nowDate := fmt.Sprintf("%d%d%d", by, bm, bd)
		if dbDate == nowDate {
			_, err := s.db.Exec("update check_services_info "+
				"set count_click = $1, day = $2 "+
				"where id = $3", countClick+1, time.Now(), id)
			if err != nil {
				return err
			}
			needInsert = false
		}
	}

	if needInsert {
		_, err := s.db.Exec("insert into check_services_info (service_name, count_click, day) "+
			"values ($1, 1, $2)", serviceName, time.Now())

		if err != nil {
			return err
		}
	}
	return nil
}

func (s *ServiceInfo) UpdateServices(serviceName string, ping int64, errorDesc error) error {
	_, err := s.db.Exec("update servises "+
		"set ping = $1, error = $2 "+
		"where name_service = $3", ping, parse.NewErrorParse(errorDesc).ParseToSqlNullString(), serviceName)
	if err != nil {
		return err
	}
	return nil
}
