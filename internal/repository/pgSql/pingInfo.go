package pgSql

import (
	"database/sql"
	"strconv"
	"time"
)

type PingInfoPg struct {
	db *sql.DB
}

func NewPingInfoPg(db *sql.DB) *PingInfoPg {
	return &PingInfoPg{db: db}
}

func (p *PingInfoPg) GetMaxPing() (string, error) {
	row := p.db.QueryRow(
		"SELECT  name_service, ping " +
			"FROM servises " +
			"Where error is null " +
			"AND ping = (SELECT max(ping) FROM servises)")
	var service string
	var maxPing int64
	if err := row.Scan(&service, &maxPing); err != nil {
		return "", err
	}
	return service + " - " + strconv.FormatInt(maxPing/int64(time.Millisecond), 10) + "ms", nil
}

func (p *PingInfoPg) GetMinPing() (string, error) {
	row := p.db.QueryRow(
		"SELECT  name_service, ping " +
			"FROM servises " +
			"Where error is null " +
			"AND ping = (SELECT min(ping) FROM servises)")
	var service string
	var minPing int64
	if err := row.Scan(&service, &minPing); err != nil {
		return "", err
	}
	return service + " - " + strconv.FormatInt(minPing/int64(time.Millisecond), 10) + "ms", nil
}
