package pgSql

import (
	"database/sql"
	"fmt"
	"scrapper/internal/repository/types"
	"time"
)

type Statistic struct {
	db *sql.DB
}

func NewStatistic(db *sql.DB) *Statistic {
	return &Statistic{db: db}
}

func (s *Statistic) GetErrorServices() ([]types.ErrorService, error) {
	var rows *sql.Rows
	var err error

	rows, err = s.db.Query(
		"SELECT  name_service, error " +
			"FROM servises " +
			"Where error is not null")

	if err != nil {
		return nil, err
	}

	var errorServices []types.ErrorService

	for rows.Next() {
		es := types.ErrorService{}
		err := rows.Scan(&es.ServiceName, &es.ErrorDesc)
		if err != nil {
			fmt.Println(err)
			continue
		}
		errorServices = append(errorServices, es)
	}

	return errorServices, nil
}

func (s *Statistic) GetCheckStatistic() ([]types.CheckStatistic, error) {
	var rows *sql.Rows
	var err error

	rows, err = s.db.Query(
		"SELECT  service_name, count_click, day " +
			"FROM check_services_info")

	if err != nil {
		return nil, err
	}

	var checkStats []types.CheckStatistic

	for rows.Next() {
		cs := types.CheckStatistic{}
		err := rows.Scan(&cs.ServiceName, &cs.CountCheck, &cs.Date)
		if err != nil {
			fmt.Println(err)
			continue
		}
		checkStats = append(checkStats, cs)

	}
	return checkStats, nil
}

func (s *Statistic) RemoveOldStatistic() error {
	_, err := s.db.Exec(
		"delete from check_services_info "+
			"where day < $1", time.Now().Add(-(24*7)*time.Hour))
	if err != nil {
		return err
	}
	return nil
}
