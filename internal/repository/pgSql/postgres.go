package pgSql

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
)

type Config struct {
	UserName string
	Password string
	DbName   string
	SslMode  string
	Host     string
	Port     string
}

func NewPostgresDb(conf Config) (*sql.DB, error) {
	db, err := sql.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		conf.Host, conf.Port, conf.UserName, conf.Password, conf.DbName, conf.SslMode))
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}
	return db, nil
}
