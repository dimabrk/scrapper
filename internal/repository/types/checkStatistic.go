package types

type CheckStatistic struct {
	ServiceName string
	CountCheck  int64
	Date        string
}
