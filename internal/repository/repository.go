package repository

import (
	"database/sql"
	"scrapper/internal/repository/pgSql"
	"scrapper/internal/repository/types"
)

type ToDoStatistic interface {
	GetErrorServices() ([]types.ErrorService, error)
	GetCheckStatistic() ([]types.CheckStatistic, error)
	RemoveOldStatistic() error
}

type ToDoPingInfo interface {
	GetMaxPing() (string, error)
	GetMinPing() (string, error)
}

type ToDoService interface {
	ServiceIsSupport(string) (bool, error)
	GetServices() ([]string, error)
	UpdateCheckServiceInfo(string) error
	UpdateServices(string, int64, error) error
}

type Repository struct {
	ToDoPingInfo
	ToDoStatistic
	ToDoService
}

func NewRepository(db *sql.DB, limit int, offset int) *Repository {
	return &Repository{
		ToDoPingInfo:  pgSql.NewPingInfoPg(db),
		ToDoService:   pgSql.NewServiceInfoPg(db, limit, offset),
		ToDoStatistic: pgSql.NewStatistic(db)}
}
