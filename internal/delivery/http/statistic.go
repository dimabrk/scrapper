package http

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// @Summary Check statistic
// @Tags Statistic
// Produce json
// @Failure 500 {string} http.StatusInternalServerError
// @Success 200 {integer} integer 1
// @Router /pingInfo/statistic/request [get]
func (h *Handler) getStatisticRequest(w http.ResponseWriter, r *http.Request) {
	list, err := h.services.GetCheckStatistic()
	if err != nil {
		log.Printf("Error while getting statistic: %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else {
		for _, item := range list {
			b, err := json.Marshal(item)
			if err != nil {
				log.Printf("Error serialize json: %s", err.Error())
				http.Error(w, err.Error(), http.StatusInternalServerError)
			} else {
				fmt.Fprint(w, string(b)+"\n")
			}
		}
	}
}

// @Summary Error list
// @Tags Statistic
// Produce json
// @Failure 500 {string} http.StatusInternalServerError
// @Success 200 {integer} integer 1
// @Router /pingInfo/statistic/error [get]
func (h *Handler) getErrorList(w http.ResponseWriter, r *http.Request) {
	list, err := h.services.GetErrorServices()
	if err != nil {
		log.Printf("Error while getting errors statistic: %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else {
		for _, item := range list {
			b, err := json.Marshal(item)
			if err != nil {
				log.Printf("Error serialize json: %s", err.Error())
				http.Error(w, err.Error(), http.StatusInternalServerError)
			} else {
				fmt.Fprint(w, string(b)+"\n")
			}
		}
	}
}
