package http

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"scrapper/pkg/time"
)

// @Summary Returned max ping
// @Tags Ping info
// @Produce json
// @Failure 500 {string} http.StatusInternalServerError
// @Success 200 {integer} integer 1
// @Router /pingInfo/command/maxPing [get]
func (h *Handler) getMaxPing(w http.ResponseWriter, r *http.Request) {
	maxPing, err := h.services.GetMaxPing()
	if err != nil {
		log.Printf("Error while getting max ping: %s", err.Error())
		http.Error(w,err.Error(), http.StatusInternalServerError)
	} else {
		err := h.services.UpdateCheckServiceInfo("maxPing")
		if err != nil {
			log.Fatalf("Error while update service info: %s", err.Error())
		}
		fmt.Fprint(w, maxPing)
	}
}

// @Summary Returned min ping
// @Tags Ping info
// @Produce json
// @Failure 500 {string} http.StatusInternalServerError
// @Success 200 {integer} integer 1
// @Router /pingInfo/command/minPing [get]
func (h *Handler) getMinPing(w http.ResponseWriter, r *http.Request) {
	minPing, err := h.services.GetMinPing()
	if err != nil {
		log.Println("Error while getting min ping: %s", err.Error())
		http.Error(w,err.Error(), http.StatusInternalServerError)
	} else {
		err := h.services.UpdateCheckServiceInfo("minPing")
		if err != nil {
			log.Printf("Error while update service info: %s", err.Error())
		}
		fmt.Fprint(w, minPing)
	}
}

// @Summary Returned ping
// @Tags Ping info
// Produce json
// Accept json
// @Param service path string true "Service name (example: google.com)"
// @Failure 500 {string} http.StatusInternalServerError
// @Failure 400 {string} Service not supported
// @Success 200 {integer} integer 1
// @Router /pingInfo/service/{service} [get]
func (h *Handler) getPingService(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	service := vars["serviceName"]
	serviceIsSupported, err := h.services.ToDoService.ServiceIsSupport(service)
	if err != nil {
		log.Printf("Error when checking supported services: %s", err.Error())
		http.Error(w,err.Error(), http.StatusInternalServerError)
	}
	if serviceIsSupported {
		pinger := time.NewPinger()
		ping, err := pinger.GetPing(service)
		if err != nil {
			log.Printf("Error when checking ping service: %s", err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		errUpdate := h.services.UpdateCheckServiceInfo(service)
		if errUpdate != nil {
			log.Printf("Error while update service info: %s", err.Error())
		}
		fmt.Fprint(w, fmt.Sprintf("service - %s; ping - %s",
			service, ping))
	} else {
		log.Printf("Error: service: %s - not supported\n", service)
		http.Error(w, "Error: service not supported", http.StatusBadRequest)
	}
}
