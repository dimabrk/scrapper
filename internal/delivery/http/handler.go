package http

import (
	"github.com/gorilla/mux"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "scrapper/docs"
	"scrapper/internal/service"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

func (h *Handler) InitRoutes() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/pingInfo/service/{serviceName}", h.getPingService).Methods("GET")
	router.HandleFunc("/pingInfo/command/maxPing", h.getMaxPing).Methods("GET")
	router.HandleFunc("/pingInfo/command/minPing", h.getMinPing).Methods("GET")
	router.HandleFunc("/pingInfo/statistic/error", h.getErrorList).Methods("GET")
	router.HandleFunc("/pingInfo/statistic/request", h.getStatisticRequest).Methods("GET")
	router.PathPrefix("/swagger/").Handler(httpSwagger.WrapHandler)
	return router
}
