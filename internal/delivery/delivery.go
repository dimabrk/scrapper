package delivery

import (
	"github.com/gorilla/mux"
	"scrapper/internal/delivery/http"
	"scrapper/internal/service"
)

type Delivery struct {
	HttpHandler
}

type HttpHandler interface {
	InitRoutes() *mux.Router
}

func NewDelivery(services *service.Service) *Delivery {
	return &Delivery{HttpHandler: http.NewHandler(services)}
}
