package http

import (
	"log"
	"net/http"
	"time"
)

type Server struct {
	httpServer *http.Server
}

func NewHttpServer(httpServer *http.Server) *Server {
	return &Server{httpServer: httpServer}
}

func (s *Server) Run(port string, handler http.Handler) error {
	s.httpServer = &http.Server{
		Addr:           ":" + port,
		Handler:        handler,
		MaxHeaderBytes: 1 << 20, // 1 MB
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
	}
	log.Println("http server started")
	return s.httpServer.ListenAndServe()
}
