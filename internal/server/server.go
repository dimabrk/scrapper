package server

import (
	"net/http"
	httpSrv "scrapper/internal/server/http"
)

type Server struct {
	HttpServer
}

type HttpServer interface {
	Run(port string, handler http.Handler) error
}

func NewServer() *Server {
	return &Server{HttpServer: httpSrv.NewHttpServer(new(http.Server))}
}
