module scrapper

go 1.16

require (
	github.com/claudiu/gocron v0.0.0-20151103142354-980c96bf412b
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.0
	github.com/swaggo/http-swagger v1.0.0
	github.com/swaggo/swag v1.7.0
)
