FROM golang:1.14-buster

RUN go version
ENV GOPATH=/

COPY ./ ./

# install psql
RUN apt-get update
RUN apt-get -y install postgresql-client

# make wait-for-postgres.sh executable
RUN chmod +x wait-for-postgres.sh

# build go app
RUN go mod download
RUN go build -o scrapper ./cmd/app/main.go
RUN go build -o scrapper_cliner ./cmd/schedulers/cline/main.go
RUN go build -o scrapper_pinger ./cmd/schedulers/ping/main.go
CMD ["./scrapper"]
